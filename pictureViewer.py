#! /usr/bin/python3

import glob
import random
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import os
import pickle
import sys
import getopt
import datetime


max_possible_year = datetime.datetime.now().year
min_possible_year = 1969
save_file = "/tmp/saved_paths.p"
dir_path = "/opt/Drive/GoogleDrive/Family_Pics"
image_dir = '/srv/http/slideshow/image'
if (os.path.exists(image_dir) is False):
    os.makedirs(image_dir)
os.chmod(image_dir, 0o777)
temp_file = os.path.join(image_dir, "image.jpg")

exts = ['*.jpg', '*.JPG', '*.jpeg', '*.JPEG']


def listOfFiles(my_path):
    files = []
    if os.path.exists(save_file):
        files = pickle.load(open(save_file, "rb"))
    if (len(files) < 1):
        files = [f for ext in exts for f in
                 glob.glob(os.path.join(dir_path, '**', ext), recursive=True)]
        pickle.dump(files, open(save_file, "wb"))
    return files

def selectFiles(beginning_year, ending_year, list):
   max_possible_year = datetime.datetime.now().year
   finalListOfFiles = [""]
   listOfYears = range (beginning_year, ending_year)
   if beginning_year > 0 and ending_year > 0:
        if beginning_year < 1900:
            print ("There are no photos possible before " + str(min_possible_year))
            return finalListOfFiles
        if ending_year > (beginning_year + 1) and ending_year > max_possible_year:
            print ("There are no possible pictures after " +  str(max_possible_year))
            return finalListOfFiles
        if (beginning_year > 1900):
            for year in listOfYears:
                for file in list:
                    yearString = '/' + str(year) + '/'
                    fileIsInYear = yearString in str(file)
                    if fileIsInYear:
                        finalListOfFiles.append(file)
                if (beginning_year == ending_year):
                    return list
   return finalListOfFiles

def getImage(beginning_year, ending_year):
    random.seed()
    if os.path.exists(temp_file):
        os.remove(temp_file)
    initialListOfFiles = listOfFiles(dir_path)
    list = selectFiles(beginning_year, ending_year, initialListOfFiles)
    if len(list) < 2:
        list = initialListOfFiles
    number_of_pictures_found = len(list)
    print("There were " + str(number_of_pictures_found) + " pictures found.")
    randomIndex = random.randint(0, number_of_pictures_found-1)
    image = list[randomIndex]
    if not (os.path.exists(image)):
        list = listOfFiles(dir_path)
        number_of_pictures_found = len(list)
        randomIndex = random.randint(0, number_of_pictures_found-1)
        image = list[randomIndex]
    list_of_dirs = image.split('/')
    short_path = list_of_dirs[-3] + '/' + list_of_dirs[-2] + \
        '/' + list_of_dirs[-1]
    howManyLeft = str(randomIndex) + "/" + str(number_of_pictures_found)
    reportString = howManyLeft + "\n" + short_path
    im = Image.open(image)
    draw = ImageDraw.Draw(im)
    height_of_picture, width_of_picture = im.size
    nom_font_size = 20
    scale_to = 480
    scaled_font = int(nom_font_size * (height_of_picture / scale_to))
    font = ImageFont.truetype("Vera.ttf", scaled_font)
    draw.text((0, 0), reportString, (255, 215, 0), font=font)
    im.save(temp_file)
    list.remove(image)
    pickle.dump(list, open(save_file, "wb"))


def main(argv):
    ending_year = -1
    beginning_year = -1
    errorMessage = 'pictureViewer.py -b <beginning year> -e <ending year>\n' \
                   'or pictureViewer.py -y <year to view'

    try:
        opts, args = getopt.getopt(argv, "hi:o", ["beginningYear=", "endingYear=",
                                                  "year=", "reset"])
    except getopt.GetoptError:
        print (errorMessage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print (errorMessage)
            sys.exit()
        elif opt in ("-y", "--year"):
            if os.path.exists(save_file):
                os.remove(save_file)
            ending_year = int(arg) + 1
            beginning_year = int(arg)
        elif opt in ("-e", "--endingYear"):
            if os.path.exists(save_file):
                os.remove(save_file)
            ending_year = int(arg)
        elif opt in ("-e", "--beginningYear"):
            if os.path.exists(save_file):
                os.remove(save_file)
            beginning_year = int(arg)
        elif opt in ("-r", "--reset"):
            os.remove(save_file)

    getImage(beginning_year, ending_year)

if __name__ == "__main__":
    main(sys.argv[1:])
